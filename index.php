<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>lpkiajaya</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <?php include_once 'link.php'; ?>
  </head>
  <body>
    <div class="main">
      <?php include_once 'header.php'; ?>

      <div class="content">
        <div class="content_resize">
          <?php include_once 'config.php'; ?>
          <?php include_once 'sidebar.php'; ?>
          <div class="clr"></div>
        </div>
      </div>

      <div class="fbg">
        <div class="fbg_resize">

          <div class="col c1">
            <h2><span>Image</span> Gallery</h2>
            <a href="#">
              <img src="images/gambar1.jpg" width="75" height="75" alt="" class="gal" />
            </a>
            <a href="#">
              <img src="images/gam2.jpg" width="75" height="75" alt="" class="gal" />
            </a>
            <a href="#">
              <img src="images/gam3.jpg" width="75" height="75" alt="" class="gal" />
            </a>
            <a href="#">
              <img src="images/gam4.jpg" width="75" height="75" alt="" class="gal" />
            </a>
            <a href="#">
              <img src="images/gam5.jpg" width="75" height="75" alt="" class="gal" />
            </a>
            <a href="#">
              <img src="images/gam6.jpg" width="75" height="75" alt="" class="gal" />
            </a>
          </div>

          <div class="col c2">
            <h2><span>Info</span> </h2>
          
            <ul class="fbg_ul">
              <li><a href="daftarmhsbaru">Daftar Mahasiswa Baru</a></li>
              <li><a href="hasilseleksi">Hasil Tes Seleksi</a></li>
             
            </ul>
          </div>

          <div class="col c3">
            <h2><span>Contact</span> Us</h2>
            <p>Jl. Soekarno-Hatta No.456, Kota Bandung, Jawa Barat 4026</p>
            <p class="contact_info">
             
              <span>Phone:</span> 0896551142139(Putri Asiyahh)<br />
              <span>E-mail:</span> <a href="#">lpkiajaya1984@lpkia.ac.id</a>
            </p>
          </div>
          <div class="clr"></div>

        </div>
      </div>

      <?php include_once 'footer.php'; ?>
    </div>
  </body>
</html>