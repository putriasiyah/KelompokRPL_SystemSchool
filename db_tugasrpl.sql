-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 01 Jul 2017 pada 15.19
-- Versi Server: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_tugasrpl`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `absen`
--

CREATE TABLE `absen` (
  `nama_matkul` varchar(20) NOT NULL,
  `sesi_kuliah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `beasiswa`
--

CREATE TABLE `beasiswa` (
  `mahasiswa` int(11) NOT NULL,
  `nim` int(11) NOT NULL,
  `kelas` varchar(10) NOT NULL,
  `transkip_nilai` int(11) NOT NULL,
  `sertifikat` varchar(20) NOT NULL,
  `ktm` int(11) NOT NULL,
  `surat_pernyataan` varchar(20) NOT NULL,
  `surat_permintaan` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `dosen`
--

CREATE TABLE `dosen` (
  `nip` int(11) NOT NULL,
  `namadosen` varchar(20) NOT NULL,
  `alamat` varchar(25) NOT NULL,
  `pendidikan` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `hasil_selesksi`
--

CREATE TABLE `hasil_selesksi` (
  `no_pendaftaran` int(11) NOT NULL,
  `nama` varchar(25) NOT NULL,
  `keterangan` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `hasil_selesksi`
--

INSERT INTO `hasil_selesksi` (`no_pendaftaran`, `nama`, `keterangan`) VALUES
(35, 'Putri', 'Lulus');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jadwal`
--

CREATE TABLE `jadwal` (
  `hari` varchar(20) NOT NULL,
  `kelas` char(4) NOT NULL,
  `no_ruang` int(11) NOT NULL,
  `jam_kuliah` time NOT NULL,
  `nama_matakuliah` varchar(20) NOT NULL,
  `nip` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kelas`
--

CREATE TABLE `kelas` (
  `no_ruang` int(11) NOT NULL,
  `lantai` int(11) NOT NULL,
  `jenis` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kreasi`
--

CREATE TABLE `kreasi` (
  `transkip` int(11) NOT NULL,
  `sertifikat` varchar(20) NOT NULL,
  `surat_pernyataan` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kreasimhs`
--

CREATE TABLE `kreasimhs` (
  `no_kreasi` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `judul_kreasi` varchar(50) NOT NULL,
  `jenis_kreasi` varchar(50) NOT NULL,
  `tanggal` datetime NOT NULL,
  `kreasi` longblob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `login`
--

CREATE TABLE `login` (
  `no_login` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `hak_akses` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `login`
--

INSERT INTO `login` (`no_login`, `username`, `password`, `hak_akses`) VALUES
(1, '3314059', 'lpkiajaya1984', 'mahasiswa'),
(2, 'andini', 'andini', 'dosen'),
(3, 'budi', 'budi', 'petugas1'),
(4, 'mita', 'mita12345', 'petugas2');

-- --------------------------------------------------------

--
-- Struktur dari tabel `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `nrp` int(11) NOT NULL,
  `nama` varchar(25) NOT NULL,
  `kelas` char(4) NOT NULL,
  `prodi` varchar(20) NOT NULL,
  `jurusan` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `matkul`
--

CREATE TABLE `matkul` (
  `kode` int(11) NOT NULL,
  `namamatkul` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `nilai`
--

CREATE TABLE `nilai` (
  `nrp` int(11) NOT NULL,
  `namamatkul` varchar(50) NOT NULL,
  `tugas` varchar(20) NOT NULL,
  `uts` int(11) NOT NULL,
  `uas` int(11) NOT NULL,
  `na` int(11) NOT NULL,
  `ipk` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pendaftaran`
--

CREATE TABLE `pendaftaran` (
  `no_pendaftaran` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `alamat_sekarang` varchar(100) NOT NULL,
  `kota` varchar(50) NOT NULL,
  `kode_pos` int(11) NOT NULL,
  `ttl` date NOT NULL,
  `agama` varchar(10) NOT NULL,
  `jenis_kelamin` varchar(10) NOT NULL,
  `kewarganegaraan` varchar(50) NOT NULL,
  `no_telepon` varchar(13) NOT NULL,
  `pendidikan_terakhir` varchar(50) NOT NULL,
  `nama_sekolah` varchar(50) NOT NULL,
  `alamat_sekolah` varchar(100) NOT NULL,
  `jurusan_sekolah` varchar(50) NOT NULL,
  `tahun_lulus` int(11) NOT NULL,
  `nama_pekerjaan` varchar(100) NOT NULL,
  `alamat_kerja` varchar(100) NOT NULL,
  `nama_ayah` varchar(50) NOT NULL,
  `nama_ibu` varchar(50) NOT NULL,
  `alamat_ortu` varchar(100) NOT NULL,
  `gaji_ortu` int(11) NOT NULL,
  `jurusan` varchar(30) NOT NULL,
  `jenis` varchar(30) NOT NULL,
  `jenjang` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `spp`
--

CREATE TABLE `spp` (
  `nrp` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `semester` int(11) NOT NULL,
  `nominal` int(11) NOT NULL,
  `keterangan` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `hasil_selesksi`
--
ALTER TABLE `hasil_selesksi`
  ADD PRIMARY KEY (`no_pendaftaran`);

--
-- Indexes for table `kreasimhs`
--
ALTER TABLE `kreasimhs`
  ADD PRIMARY KEY (`no_kreasi`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`no_login`);

--
-- Indexes for table `nilai`
--
ALTER TABLE `nilai`
  ADD PRIMARY KEY (`nrp`);

--
-- Indexes for table `pendaftaran`
--
ALTER TABLE `pendaftaran`
  ADD PRIMARY KEY (`no_pendaftaran`);

--
-- Indexes for table `spp`
--
ALTER TABLE `spp`
  ADD PRIMARY KEY (`nrp`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kreasimhs`
--
ALTER TABLE `kreasimhs`
  MODIFY `no_kreasi` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pendaftaran`
--
ALTER TABLE `pendaftaran`
  MODIFY `no_pendaftaran` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
