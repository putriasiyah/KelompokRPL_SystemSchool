<div class="mainbar">

  <div class="article">
    <h2><span>Pendaftaran Mahasiswa Baru</span></span></h2>

    <form action="" method="POST">
      <table>
        <tr>
          <td>
            No Pendaftaran 
          </td>
          <br>
          <td>
            <input type="text" name="nopndaftaran" id="nopndaftaran" placeholder="No Pendaftaran" />
          </td>
        </tr>
        <br>
        <tr>
          <td>
            Nama  
          </td>
          <td>
            <input type="text" name="nama" id="nm" placeholder="Nama Lengkap" />
          </td>
        </tr>
        <tr>
          <td>
            AlamatSekarang
          </td>
          <td>
            <input type="text" name="alamat" id="alamat" placeholder="Alamat Sekarang" />
          </td>
        </tr>
        <tr>
          <td>
            Kota
          </td>
          <td>
            <input type="text" name="kota" id="kota" placeholder="Kota" />
          </td>
        </tr>

        <tr>
          <td>Kode Pos </td>
            
           <td>
            <input type="text" name="kodepos" id="kodepos" placeholder="Kode Pos" />
          </td>
        </tr>

        <tr>
          <td>
            Tempat Tanggal Lahir
          </td>
          <td>
            <input id="dateDefault" type="date" >
            <script type="text/javascript">
              function setInputDate(_id){
              var _dat = document.querySelector(_id);
              var hoy = new Date(),
                  d = hoy.getDate(),
                  m = hoy.getMonth()+1, 
                  y = hoy.getFullYear(),
                  data;

              if(d < 10){
                  d = "0"+d;
              };
              if(m < 10){
                  m = "0"+m;
              };

              data = y+"-"+m+"-"+d;
              console.log(data);
              _dat.value = data;
              };

              setInputDate("#dateDefault");
            </script>
          </td>
        </tr>

         <tr>
          <td>
            Agama
          </td>
          <td>
          <select name="agama">
          <option value="">Silahkan Pilih</option>
           <option value="">Islam</option>
           <option value="">Kristen</option>
           <option value="">Budha</option>
            <option value="">Hindu</option>
             <option value="">Lainnya</option>
             </select>
             </td>
             </tr>

             <tr>
          <td>
            Jenis Kelamin
          </td>
          <td>
          <select name="jk">
          <option value="">Silahkan Pilih</option>
           <option value="">Perempuan</option>
           <option value="">Laki-Laki</option>
             </select>
             </td>
             </tr>

        <tr>
          <td>Kewarganegaraan</td>
            
           <td>
            <input type="text" name="warga" id="warga" placeholder="Kewarganegaraan" />
          </td>
        </tr>

        <tr>
          <td>No Telepon</td>
            
           <td>
            <input type="text" name="notlp" id="notlp" placeholder="No Telepon" />
          </td>
        </tr>
        
        <tr>
          <td>Pendidikan Terakhir</td>
            
           <td>
            <input type="text" name="pdd" id="pdd" placeholder="Pendidikan Terakhir " />
          </td>
        </tr>

        <tr>
          <td>Nama Sekolah</td>
            
           <td>
            <input type="text" name="nmsekolah" id="nmsekolah" placeholder="Nama Sekolah" />
          </td>
        </tr>

        <tr>
          <td>Alamat Sekolah</td>
            
           <td>
            <input type="text" name="alamatsekolah" id="alamatsekolah" placeholder="Alamat Sekolah" />
          </td>
        </tr>

        <tr>
          <td>Tahun Lulus</td>
            
           <td>
            <input type="text" name="tahun" id="tahun" placeholder="Tahun Lulus" />
          </td>
        </tr>

        <tr>
          <td>Nama Pekerjaan</td>
            
           <td>
            <input type="text" name="namakerja" id="nmkerja" placeholder="Nama Pekerjaan" />
          </td>
        </tr>

        <tr>
          <td>Alamat Kantor</td>
            
           <td>
            <input type="text" name="alamatkantor" id="alamatkantor" placeholder="Alamat Kantor" />
          </td>
        </tr>



        <tr>
          <td>Nama Ayah</td>
            
           <td>
            <input type="text" name="nmayah" id="nmayah" placeholder="Nama Ayah" />
          </td>
        </tr>

        <tr>
          <td>Nama Ibu</td>
            
           <td>
            <input type="text" name="nmibu" id="nmibu" placeholder="Nama Ibu" />
          </td>
        </tr>

        <tr>
          <td>Alamat Orang Tua</td>
            
           <td>
            <input type="text" name="alamatortu" id="alamatortu" placeholder="Alamat Orang Tua" />
          </td>
        </tr>

        <tr>
          <td>Gaji Orang Tua</td>
            
           <td>
            <input type="text" name="gaji" id="gaji" placeholder="Gaji Orang Tua" />
          </td>
        </tr>

        <tr>
          <td>
            Jurusan
          </td>
          <td>
          <select name="jurusan">
          <option value="">Silahkan Pilih</option>
           <option value="">Teknik Informatika</option>
           <option value="">Sistem Informasi</option>
           <option value="">Komputer Akuntansi</option>
            <option value="">Administrasi Bisnis</option>
            <option value="">Akuntansi</option>
             <option value="">Lainnya</option>
             </select>
             </td>
             </tr>

             <tr>
          <td>
            Jenis 
          </td>
          <td>
          <select name="jk">
          <option value="">Reguler</option>
           <option value="">Ekstensi</option>
             </select>
             </td>
             </tr>

             <tr>
          <td>
            Jenjang Pendidikan
          </td>
          <td>
          <select name="jk">
          <option value="">D3</option>
           <option value="">S1</option>
             </select>
             </td>
             </tr>


       <!--  <tr>
          <td>
            Jenis Organisasi
          </td>
          <td>
            <input type="text" name="email" id="email" placeholder="E-mail" />
          </td>
        </tr> -->
       <!--  <tr> -->
          <!-- <input type="combobox" name="combo" id="combo" placeholder="Pili" />
          <br><br> -->
         <!--  <td>
            Upload Logo 
          </td>
          <td>
            <input type="file" name="logo" id="lg" />
          </td>
        </tr>
        <tr>
          <td>
            Struktur Organisasi 
          </td>
          <td>
            <input type="file" name="struktur_organisasi" id="strk" />
          </td>
        </tr> -->
      </table>
      <br>
      <br>
      <input type="submit" name="submit" value="DAFTAR">
      <br>
      <br>

       <input type="reset" name="batal" value="BATAL">
    </form>
    
  </div>
</div>