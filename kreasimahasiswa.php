
  <div class="article">
    <h2><span>Form Kreasi Mahasiswa</span></span></h2>

    <form action="" method="POST">
      <table>
        <tr>
          <td>
            Nama
          </td>
          <br>
          <td>
            <input type="text" name="nama" id="nama" placeholder="Nama" />
          </td>
        </tr>

        <tr>
          <td>
            Judul Kreasi
          </td>
          <br>
          <td>
            <input type="text" name="judul" id="judul" placeholder="Judul" />
          </td>
        </tr>

         <tr>
          <td>
            Jenis Kreasi 
          </td>
          <td>
          <select name="jenis">
          <option value="">Silahkan Pilih</option>
           <option value="">Jurnal</option>
           <option value="">Video</option>
             <option value="">Lainnya</option>
             </select>
             </td>
             </tr>

             <tr>
          <td>
            Tanggal Upload
          </td>
          <td>
            <input id="dateDefault" type="date" >
            <script type="text/javascript">
              function setInputDate(_id){
              var _dat = document.querySelector(_id);
              var hoy = new Date(),
                  d = hoy.getDate(),
                  m = hoy.getMonth()+1, 
                  y = hoy.getFullYear(),
                  data;

              if(d < 10){
                  d = "0"+d;
              };
              if(m < 10){
                  m = "0"+m;
              };

              data = y+"-"+m+"-"+d;
              console.log(data);
              _dat.value = data;
              };

              setInputDate("#dateDefault");
            </script>
          </td>
        </tr>

         <tr>
          <td>
            File Kreasi
          </td>
          <td>
            <input type="file" name="ktm" id="ktm" />
          </td>
        </tr> 

         </table>
      <br>
      <br>
      <input type="submit" name="submit" value="KIRIM">
    </form>
    
  </div>
</div>