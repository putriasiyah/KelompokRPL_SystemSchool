<div class="mainbar">

  <div class="article">
    <h2><span>Pendaftaran Beasiswa</span></span></h2>

    <form action="" method="POST">
      <table>
        <tr>
          <td>
            Nama Lengkap 
          </td>
          <br>
          <td>
            <input type="text" name="nmlengkap" id="nmlengkap" placeholder="Nama Lengkap" />
          </td>
        </tr>

        <tr>
          <td>
            NIM 
          </td>
          <br>
          <td>
            <input type="text" name="nim" id="nim" placeholder="NIM" />
          </td>
        </tr>

        <tr>
          <td>
            Kelas 
          </td>
          <br>
          <td>
            <input type="text" name="kls" id="kls" placeholder="Kelas" />
          </td>
        </tr>

        <tr>
          <td>
            Transkrip Nilai 
          </td>
          <td>
            <input type="file" name="transkrip" id="transkrip" />
          </td>
        </tr> 

        <tr>
          <td>
            Sertifikat 
          </td>
          <td>
            <input type="file" name="sertifikat" id="srtf" />
          </td>
        </tr> 

        <tr>
          <td>
            KTM
          </td>
          <td>
            <input type="file" name="ktm" id="ktm" />
          </td>
        </tr> 

        <tr>
          <td>
            Surat Pernyataan
          </td>
          <td>
            <input type="file" name="suratper" id="suratper" />
          </td>
        </tr> 

        <tr>
          <td>
            Surat Permintaan
          </td>
          <td>
            <input type="file" name="suratpen" id="suratpen" />
          </td>
        </tr> 

        </table>
      <br>
      <br>
      <input type="submit" name="submit" value="DAFTAR BEASISWA">
    </form>
    
  </div>
</div>